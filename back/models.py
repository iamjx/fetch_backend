from django.db import models

# Create your models here.


class FetchRecord(models.Model):
    ip = models.CharField(max_length=64)
    group = models.CharField(max_length=64)
    file_date = models.CharField(max_length=64)
    src_file_name = models.CharField(max_length=128)
    src_file_dir = models.CharField(max_length=128)
    src_file_path = models.CharField(max_length=128)
    dest_file_dir = models.CharField(max_length=128)
    dest_file_path = models.CharField(max_length=128)
    status = models.IntegerField()
    error_msg = models.CharField(max_length=1024)
    playbook_name = models.CharField(max_length=64)
    create_time = models.DateTimeField(auto_now_add=True)
    modify_time = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'fetch_record'
