import os
import re
import sys
import time
import json
import datetime
import requests
from collections import namedtuple
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback import CallbackBase


# ansible hosts文件路径
ANSIBLE_HOSTS_PATH = '/etc/ansible/hosts'

POST_URL = 'http://192.168.133.222:8000/back/fetch_record/post/'


class AnsibleRunner(object):
    """
    This is a General object for parallel execute modules.
    """

    def __init__(self, resource, *args, **kwargs):
        self.resource = resource
        self.inventory = None
        self.variable_manager = None
        self.loader = None
        self.options = None
        self.passwords = None
        self.callback = None
        self.__initialize_data()
        self.results_raw = {}

    def __initialize_data(self):
        """
        初始化ansible配置
        """
        Options = namedtuple('Options',
                             ['listtags', 'listtasks', 'listhosts', 'syntax', 'connection', 'module_path', 'forks',
                              'remote_user', 'private_key_file', 'ssh_common_args', 'ssh_extra_args', 'sftp_extra_args',
                              'scp_extra_args', 'become', 'become_method', 'become_user', 'verbosity', 'check', 'diff'])
        self.loader = DataLoader()
        self.options = Options(listtags=False, listtasks=False, listhosts=False, syntax=False, connection='ssh',
                               module_path=None, forks=100, remote_user='root', private_key_file=None,
                               ssh_common_args=None, ssh_extra_args=None, sftp_extra_args=None, scp_extra_args=None,
                               become=True, become_method='sudo', become_user='root', verbosity=None, check=False,
                               diff=False)
        self.passwords = dict(vault_pass='secret')
        self.inventory = InventoryManager(loader=self.loader, sources=self.resource)
        self.variable_manager = VariableManager(loader=self.loader, inventory=self.inventory)

    def run(self, host_list, module_name, module_args, ):
        """
        run module from andible ad-hoc.
        module_name: ansible module_name
        module_args: ansible module args
        """
        play_source = dict(
            name="Ansible Ad-hoc Command",
            hosts=host_list,
            gather_facts='no',
            tasks=[dict(action=dict(module=module_name, args=module_args))]
        )
        play = Play().load(play_source, variable_manager=self.variable_manager, loader=self.loader)

        tqm = None
        self.callback = ResultsCollector()
        try:
            tqm = TaskQueueManager(
                inventory=self.inventory,
                variable_manager=self.variable_manager,
                loader=self.loader,
                options=self.options,
                passwords=self.passwords,
                stdout_callback='default',
            )
            tqm._stdout_callback = self.callback
            result = tqm.run(play)
        # print self.callback
        finally:
            if tqm is not None:
                tqm.cleanup()

    def run_playbook(self, playbook_name):
        try:
            self.callback = ResultsCollector()
            playbook_file = ['./' + playbook_name]
            print(playbook_file)
            # template_file = BASE_DIR + "roles/"+ role_name + "/templates"
            if not os.path.exists(playbook_name):
                print('%s 路径不存在 ' % playbook_file)
                sys.exit()
            executor = PlaybookExecutor(
                playbooks=playbook_file, inventory=self.inventory, variable_manager=self.variable_manager,
                loader=self.loader, options=self.options, passwords=self.passwords
            )
            executor._tqm._stdout_callback = self.callback
            executor.run()
        except Exception as e:
            print("Failure in run_playbook:%s" % e)
            pass

    def get_result(self):
        self.results_raw = {'success': {}, 'failed': {}, 'unreachable': {}}
        for host, result in self.callback.host_ok.items():
            self.results_raw['success'][host] = result._result

        for host, result in self.callback.host_failed.items():
            self.results_raw['failed'][host] = result._result

        for host, result in self.callback.host_unreachable.items():
            self.results_raw['unreachable'][host] = result._result['msg']

        return self.results_raw


class ResultsCollector(CallbackBase):
    def __init__(self, *args, **kwargs):
        super(ResultsCollector, self).__init__(*args, **kwargs)
        self.host_ok = {}
        self.host_unreachable = {}
        self.host_failed = {}

    def v2_runner_on_unreachable(self, result):
        self.host_unreachable[result._host.get_name()] = result

    def v2_runner_on_ok(self, result, *args, **kwargs):
        self.host_ok[result._host.get_name()] = result

    def v2_runner_on_failed(self, result, *args, **kwargs):
        self.host_failed[result._host.get_name()] = result


def generate_playbook(hosts, group, date, src_file_name, src_file_path, dest_file_dir, play_book_name):
    """

    :param hosts: 组名或同组的ip
    :param group: 组名，当hosts是组名时两者相同
    :param date: now或年月日格式的字符串
    :return:
    """
    tpl = """
---
    - hosts: {}
      tasks:
        - name: synchronize
          synchronize:
            mode: "pull"
            checksum: yes
            src: "{}"
            dest: "{}{}/{}/{{{{ inventory_hostname }}}}.{}"
            rsync_path: "/usr/bin/rsync"
    """.format(hosts, src_file_path, dest_file_dir, date, group, src_file_name)
    with open(play_book_name, 'w') as f:
        f.write(tpl)


def get_cfg():
    with open('./main_cfg.json') as f:
        return json.loads(f.read())


def alert():
    pass


def post_res(ip, group, file_date, src_file_name, src_file_dir, src_file_path, dest_file_dir, dest_file_path, status, error_msg, playbook_name, r_id):
    res = {
            "ip": ip,
            "group": group,
            "file_date": file_date,
            "src_file_name": src_file_name,
            "src_file_dir": src_file_dir,
            "src_file_path": src_file_path,
            "dest_file_dir": dest_file_dir,
            "dest_file_path": dest_file_path,
            "status": status,
            "error_msg": error_msg,
            "playbook_name": playbook_name,
            "r_id": r_id,
    }
    r = requests.post(POST_URL, data=res)


def run(hosts, group, cfg, src_file_name, date, r_id=-1):
    """

    :param group_or_host_info: 字典类型, 详细格式见main_cfg.json
    :param date: now或者YYYY-MM-DD的日期格式
    :return:
    """
    if date == 'now':
        date = time.strftime('%Y-%m-%d', time.localtime())
        src_file_path = cfg['src_file_dir'] + src_file_name
    else:
        src_file_path = cfg['src_file_dir'] + src_file_name + '.' + date + '.log'
    playbook_name = '{}_{}'.format(time.time(), cfg['playbook_name'])
    generate_playbook(hosts, group, date, src_file_name, src_file_path, cfg['dest_file_dir'], playbook_name)
    if not os.path.exists(cfg['dest_file_dir'] + date + '/' + group):
        os.system('mkdir -p {}'.format(cfg['dest_file_dir'] + date + '/' + group))
    a = AnsibleRunner(ANSIBLE_HOSTS_PATH)
    a.run_playbook(playbook_name)
    res_detail = a.get_result()
    print(res_detail)
    failed_host = res_detail['failed'].keys()
    unreachable_host = res_detail['unreachable'].keys()
    success_host = list(set(res_detail['success'].keys()) -
                   set(failed_host) -
                   set(unreachable_host))
    for success in success_host:
        post_res(success, group, date,
                 src_file_name,
                 cfg['src_file_dir'],
                 cfg['src_file_dir'] + src_file_name,
                 cfg['dest_file_dir'],
                 cfg['dest_file_dir'] + date + '/' + group + '/' + success + '.' + src_file_name,
                 0, "", cfg['playbook_name'], r_id)
    for fail in failed_host:
        print('{}: {}'.format(fail, res_detail['failed'][fail]['msg']))
        post_res(fail, group, date,
                 src_file_name,
                 cfg['src_file_dir'],
                 cfg['src_file_dir'] + src_file_name,
                 cfg['dest_file_dir'],
                 cfg['dest_file_dir'] + date + '/' + group + '/' + fail + '.' + src_file_name,
                 -1, res_detail['failed'][fail]['msg'], cfg['playbook_name'], r_id)
    for unreachable in unreachable_host:
        print('{}: {}'.format(unreachable, res_detail['unreachable'][unreachable]))
        post_res(unreachable, group, date,
                 src_file_name,
                 cfg['src_file_dir'],
                 cfg['src_file_dir'] + src_file_name,
                 cfg['dest_file_dir'],
                 cfg['dest_file_dir'] + date + '/' + group + '/' + unreachable + '.' + src_file_name,
                 1, res_detail['unreachable'][unreachable], cfg['playbook_name'], r_id)
    os.system('rm -f ./{}'.format(playbook_name))


if __name__ == '__main__':
    configure = get_cfg()
    today = datetime.datetime.now()
    yesterday = (today - datetime.timedelta(days=1))
    yesterday_date = datetime.date(yesterday.year, yesterday.month, yesterday.day)
    for group, cfg in configure.items():
        for file in cfg['src_file_name_list']:
            run(group, group, cfg, file, yesterday_date.isoformat())

