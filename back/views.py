import threading
from django.shortcuts import render, redirect, reverse
from back.models import FetchRecord
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from ansible_scripts.fetch_log import run
# Create your views here.


def back_index(request):
    all_records = FetchRecord.objects.all().order_by('-create_time')
    return render(request, 'back/index.html', context={'records': all_records})


def retry_index(request, r_id):
    record = FetchRecord.objects.get(pk=r_id)
    return render(request, 'back/retry.html', context={'record': record})


def retry_process(request, r_id):
    ip = request.POST.get('ip')
    group = request.POST.get('group')
    file_date = request.POST.get('file_date')
    src_file_name = request.POST.get('src_file_name')
    src_file_dir = request.POST.get('src_file_dir')
    dest_file_dir = request.POST.get('dest_file_dir')
    playbook_name = request.POST.get('playbook_name')
    cfg = {
        "src_file_dir": src_file_dir,
        "dest_file_dir": dest_file_dir,
        "playbook_name": playbook_name
    }
    fr = FetchRecord.objects.get(pk=r_id)
    fr.status = 2
    fr.save()
    t = threading.Thread(target=run, args=(ip, group, cfg, src_file_name, file_date, r_id))
    t.start()
    return redirect(reverse('back:back_index'))


@require_http_methods('POST')
@csrf_exempt
def api_data(request):
    ip = request.POST.get('ip')
    group = request.POST.get('group')
    file_date = request.POST.get('file_date')
    src_file_name = request.POST.get('src_file_name')
    src_file_dir = request.POST.get('src_file_dir')
    src_file_path = request.POST.get('src_file_path')
    dest_file_dir = request.POST.get('dest_file_dir')
    dest_file_path = request.POST.get('dest_file_path')
    status = request.POST.get('status')
    error_msg = request.POST.get('error_msg')
    playbook_name = request.POST.get('playbook_name')
    r_id = request.POST.get('r_id')
    if r_id == '-1':
        fr = FetchRecord(
            ip=ip,
            group=group,
            file_date=file_date,
            src_file_name=src_file_name,
            src_file_dir=src_file_dir,
            src_file_path=src_file_path,
            dest_file_dir=dest_file_dir,
            dest_file_path=dest_file_path,
            status=status,
            error_msg=error_msg,
            playbook_name=playbook_name
        )
        fr.save()
    else:
        fr = FetchRecord.objects.get(pk=r_id)
        fr.ip = ip
        fr.group = group
        fr.file_date = file_date
        fr.src_file_name = src_file_name
        fr.src_file_dir = src_file_dir
        fr.src_file_path = src_file_path
        fr.dest_file_dir = dest_file_dir
        fr.dest_file_path = dest_file_path
        fr.status = status
        fr.error_msg = error_msg
        fr.playbook_name = playbook_name
        fr.save()
    return HttpResponse('ok')
