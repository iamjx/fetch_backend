from django.urls import path
import back.views

app_name = 'back'

urlpatterns = [
    path('fetch_record/post/', back.views.api_data),
    path('', back.views.back_index, name='back_index'),
    path('retry/<int:r_id>', back.views.retry_index, name='back_retry'),
    path('retry/process/<int:r_id>', back.views.retry_process, name='back_retry_process')
]